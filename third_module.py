import json
import re
import sys
import requests
from lxml import html
from xml.etree import ElementTree
from urllib.parse import urlparse
from first_module import InheritanceEmulator


# Step 3.2.6
# Count number of possible replaces
def count_replace(text, old, new):
    if old not in text:
        return 0
    count = 1
    while count < 1000:
        text = text.replace(old, new)
        if old not in text:
            return count
        else:
            count += 1
    return "Impossible"


# Step 3.2.7
# Count the number of intersecting entries in the string
def count_entries(text, substring):
    count = 0
    while substring in text:
        index = text.index(substring) + 1
        text = text[index:]
        count += 1
    return count


# Series of task with regular expressions
def check_string_by_pattern(pattern):
    for line in sys.stdin:
        line = line.rstrip()
        if re.search(pattern, line):
            print(line)


def replace_string_by_pattern(pattern, replace_str, count=0):
    for line in sys.stdin:
        line = line.rstrip()
        print(re.sub(pattern, replace_str, line, count))


# Step 3.3.7
# Find strings where 'cat' is found more than 2 times
double_cat_pattern = r"cat.*cat"

# Step 3.3.8
# Find strings where 'cat' is single word
cat_word_pattern = r"\bcat\b"

# Step 3.3.9
# Find strings where between two 'z' exactly three characters
three_characters_pattern = r"z.{3}z"

# Step 3.3.10
# Find strings with backslash
backslash_pattern = r"\\"

# Step 3.3.11
# Find strings with word, which consists of two identical parts
repeat_word_pattern = r"\b(\w+)\1\b"

# Step 3.3.12
# Replace "human" on "computer" in every string
human_pattern = r"human"
replace_string = "computer"

# Step 3.3.13
# Replace first word in string which contains only letters with word "argh"
first_word_pattern = r"\b[aA]+\b"
replace_string = "argh"

# Step 3.3.14
# Swap first and second character in every word in string
swap_characters_pattern = r"\b(\w)(\w)"
replace_string = r"\2\1"

# Step 3.3.15
# Replace multiple identical letters in words with one letter
one_letter_pattern = r"(\w)\1+"
replace_string = r"\1"


# Step 3.4.6
# Check that two web-pages linked via another web-page
def two_click_transition(first_url, second_url):
    first_page_content = requests.get(first_url).text
    link_pattern = re.compile(r'href="(.*)"')
    first_page_links = link_pattern.findall(first_page_content)
    for link in first_page_links:
        current_page_content = requests.get(link).text
        current_links_list = link_pattern.findall(current_page_content)
        if second_url in current_links_list:
            return "Yes"
    return "No"


# Step 3.4.7
# Find all domains which contains in links on page using lxml and urllib.parse modules
def find_domains(content):
    page = html.document_fromstring(content)
    domains = []
    for link in page.xpath('//a/@href'):
        domain = urlparse(link).hostname
        if domain and domain not in domains:
            domains.append(domain)
    return sorted(domains)


# Step 3.5.2
# Find most often type of crime from dataset
def most_often_crime(filename):
    crime_types = {}
    with open(filename) as f:
        f.readline()
        for row in f:
            line = row.split(",")
            if "2015" in line[2]:
                if line[5] in crime_types:
                    crime_types[line[5]] += 1
                else:
                    crime_types[line[5]] = 1

    return max(crime_types, key=crime_types.get)


# Step 3.5.4
# Count number of children for each class. Using modified InheritanceEmulator from first_module
def emulate_inheritance_from_json():
    classes_dict = {}
    data = json.loads(input())
    # Transform given json to appropriate format
    for item in data:
        classes_dict[item["name"]] = item["parents"]
    emulator = InheritanceEmulator(classes_dict)
    for class_name in sorted(emulator.class_relations):
        print(class_name + " : " + str(emulator.count_children(class_name)))


# Step 3.6.3
# Check interesting fact about number using API numbersapi.com
def is_interesting_number(number):
    url = "http://numbersapi.com/" + str(number) + "/math?json=true"
    req = requests.get(url)
    data = json.loads(req.text)
    return "Interesting" if data["found"] else "Boring"


def check_interesting_numbers():
    with open("data/step_3.6.3/dataset_1.txt", "r", encoding="UTF-8") as input_file:
        for line in input_file:
            print(is_interesting_number(line.strip()))


# Step 3.6.4
# Get artist by id from artsy.net and print them sorted by Date of Birth
def get_artsy_token():
    client_id = "0be148c10f678faf6e01"
    client_secret = "32a6c80bcd17605fa047125b999b1d92"
    # request for token
    r = requests.post("https://api.artsy.net/api/tokens/xapp_token",
                      data={
                          "client_id": client_id,
                          "client_secret": client_secret
                      })
    j = json.loads(r.text)
    token = j["token"]
    return token


def get_artists_from_artsy():
    token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyb2xlcyI6IiIsImV4cCI6MTU1MDUyMDc1NSwiaWF0IjoxNTQ5OTE1OTU1LCJhdWQiOi" \
            "I1YzYxZDczM2U2Y2JlMzBkMDg5ODYxM2EiLCJpc3MiOiJHcmF2aXR5IiwianRpIjoiNWM2MWQ3MzNkNjE4NTgxNWRjN2M0NjkyIn0.jdFaN" \
            "fFGG73O8_jngwUXUp7tAmHhcIhnj94UeFHps28"
    headers = {"X-Xapp-Token": token}
    artists = []
    with open("data/step_3.6.4/dataset_2.txt") as input_f:
        for row in input_f:
            request = requests.get("https://api.artsy.net/api/artists/" + row.strip(), headers=headers)
            json_data = json.loads(request.text)
            artists.append(json_data["birthday"] + json_data["sortable_name"])
    for artist in sorted(artists):
        print(artist[4:])


# Step 3.7.5
# Count weight of cubes, which depends on element position in xml
def count_cubes(color_scores, current_element, count):
    color_scores[current_element.attrib["color"]] += count
    for child in current_element:
        count_cubes(color_scores, child, count + 1)


def count_cubes_from_string():
    colors = {"red": 0, "blue": 0, "green": 0}
    root = ElementTree.fromstring(input())
    count_cubes(colors, root, count=1)
    print(colors["red"], colors["green"], colors["blue"])
