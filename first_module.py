import time

# Step 1.1.9
# Function for calculation fibonacci number
fib = lambda x: 1 if x <= 2 else fib(x - 1) + fib(x - 2)


# Step 1.1.10
# Function for sum of numbers from input
def sum_of_inputs():
    number_of_inputs = int(input())
    sum_of_numbers = sum([int(input()) for x in range(number_of_inputs)])
    return sum_of_numbers


# Step 1.2.9
# Count number of different objects in list
def number_of_different_objects(objects):
    unique_objects = set()
    for elem in objects:
        unique_objects.add(id(elem))
    return len(unique_objects)


# Step 1.3.9
# Get closest number which divisible by 5
def closest_mod_5(x):
    if x % 5 == 0:
        return x
    else:
        return x + (5 - x % 5)


# Step 1.3.15
# Calculate number of combination via recursion
def number_of_combination(n, k):
    if k == 0:
        return 1
    elif k > n:
        return 0
    else:
        return number_of_combination(n - 1, k) + number_of_combination(n - 1, k - 1)


# Step 1.4.9
# Emulate work of namespaces
class NamespaceEmulator:
    def __init__(self):
        self.variables = {'global': []}
        self.relations = {}

    def add_namespace(self, namespace, parent):
        self.variables[namespace] = []
        self.relations[namespace] = parent

    def add_var(self, namespace, var):
        self.variables[namespace].append(var)

    def find_var_namespace(self, current_namespace, var):
        while True:
            if var in self.variables[current_namespace]:
                return current_namespace
            elif current_namespace == 'global':
                return None
            else:
                current_namespace = self.relations[current_namespace]


def emulate_namespaces():
    emulator = NamespaceEmulator()
    for i in range(int(input())):
        command, namespace, arg = input().split()
        if command == 'create':
            emulator.add_namespace(namespace, arg)
        elif command == 'add':
            emulator.add_var(namespace, arg)
        elif command == 'get':
            print(emulator.find_var_namespace(namespace, arg))


# Step 1.5.8
class MoneyBox:
    def __init__(self, capacity):
        self.capacity = capacity
        self.amount = 0

    def can_add(self, number_of_coins):
        return self.capacity >= number_of_coins + self.amount

    def add(self, number_of_coins):
        self.amount += number_of_coins


# Step 1.5.9
class Buffer:
    def __init__(self):
        self.current_part = []

    def add(self, *args):
        self.current_part.extend(args)
        while len(self.current_part) >= 5:
            print(sum(self.current_part[:5]))
            del self.current_part[:5]

    def get_current_part(self):
        return self.current_part


# Step 1.6.7
# Determine whether the class is an ancestor of another class
class InheritanceEmulator:
    def __init__(self, relations={}):
        self.class_relations = relations

    def add_relation(self, new_class, *parents):
        self.class_relations[new_class] = list(parents)

    def is_parent_class(self, parent_class, child_class):
        if parent_class == child_class or parent_class in self.class_relations[child_class]:
            return True
        else:
            for p in self.class_relations[child_class]:
                if self.is_parent_class(parent_class, p):
                    return True
        return False

    def count_children(self, current_class):
        count = 0
        for key in self.class_relations:
            if self.is_parent_class(current_class, key):
                count += 1
        return count


def emulate_inheritance():
    emulator = InheritanceEmulator()
    # Reading classes relations
    for i in range(int(input())):
        row = input().split()
        if len(row) == 1:
            emulator.add_relation(row[0])
        else:
            emulator.add_relation(row[0], *row[2:])
    # Reading pairs of classes for check
    for i in range(int(input())):
        parent, child = input().split()
        print("Yes" if emulator.is_parent_class(parent, child) else "No")


# Step 1.6.8
# Class inheritance and method extension
class ExtendedStack(list):
    def sum(self):
        self.append(self.pop() + self.pop())

    def sub(self):
        self.append(self.pop() - self.pop())

    def mul(self):
        self.append(self.pop() * self.pop())

    def div(self):
        self.append(self.pop() // self.pop())


# Step 1.6.9
# Extension of class functionality by an additional class
class Loggable:
    def log(self, msg):
        print(str(time.ctime()) + ": " + str(msg))


class LoggableList(list, Loggable):
    def append(self, elem):
        list.append(self, elem)
        self.log(elem)
