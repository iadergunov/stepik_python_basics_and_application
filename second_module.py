import datetime
import simplecrypt
import os


# Step 2.1.6
# Catch different types of exceptions, taking into account the hierarchy
def foo():
    pass


try:
    foo()

except ZeroDivisionError:
    print("ZeroDivisionError")
except ArithmeticError:
    print("ArithmeticError")
except AssertionError:
    print("AssertionError")


# Step 2.1.7
# Determine if an error was caught
class ErrorEmulator:
    def __init__(self):
        self.relations = {}
        self.catched_errors = []

    def add_relation(self, error, *parents):
        self.relations[error] = list(*parents)

    def add_catched_error(self, error):
        self.catched_errors.append(error)

    def check_error(self, error):
        for parent in self.relations[error]:
            if parent in self.catched_errors:
                return True
            else:
                if self.check_error(parent):
                    return True
        return False


def emulate_catching_errors():
    emulator = ErrorEmulator()
    # Read errors relations
    for i in range(int(input())):
        row = input().split()
        if len(row) == 1:
            emulator.add_relation(row[0])
        else:
            emulator.add_relation(row[0], row[2:])
    # Read errors for check
    for i in range(int(input())):
        error = input()
        if emulator.check_error(error):
            print(error)
        emulator.add_catched_error(error)


# Step 2.1.9
# Create and catch custom exception
class NonPositiveError(Exception):
    pass


class PositiveList(list):
    def append(self, elem):
        if elem > 0:
            list.append(self, elem)
        else:
            raise NonPositiveError()


# Step 2.2.5
# Calculate date using datetime library
def calculate_date():
    date = datetime.datetime.strptime(input(), "%Y %m %d")
    date += datetime.timedelta(int(input()))
    print(date.year, date.month, date.day)
    return date


# Step 2.2.9
# Pick password and decrypt file with library simplecrypt
def decrypt():
    with open("data/step_2.2.9/encrypted.bin", "rb") as input_f:
        encrypted = input_f.read()
    with open("data/step_2.2.9/passwords.txt", "r") as input_f:
        passwords = input_f.read().split('\n')
    for password in passwords:
        try:
            decrypted = simplecrypt.decrypt(password, encrypted)
            return decrypted
        except Exception as e:
            print(e)
    return 0


# Step 2.3.4
# Class like filter, but with multiple allowing functions and ability to change judgement logic
class MultiFilter:
    def judge_half(self):
        # Allow element, if element allowed by at least half of functions (pos >= neg)
        return self.pos >= self.neg

    def judge_any(self):
        # Allow element, if element allowed by at least one function (pos >= 1)
        return self.pos >= 1

    def judge_all(self):
        # Allow element, if element allowed by all functions (neg == 0)
        return self.neg == 0

    def __init__(self, iterable, *funcs, judge=judge_any):
        # Source sequence
        self.iterable = iterable
        # Allowing functions
        self.functions = funcs
        # Judge function
        self.judge = judge

    def __iter__(self):
        # Return iterator over the resulting sequence
        for elem in self.iterable:
            check_result = [func(elem) for func in self.functions]
            self.pos, self.neg = check_result.count(True), check_result.count(False)
            if self.judge(self):
                yield elem


# Step 2.3.5
# Generator function, which generate k prime numbers
def primes():
    current_number = 2
    while True:
        is_prime = True

        for i in range(2, int(current_number ** 0.5) + 1):
            if current_number % i == 0:
                is_prime = False
                break

        if is_prime:
            yield current_number
        current_number += 1


# Step 2.4.4
# Read file and write to another with reverse line order
def reverse_file(input_filename, output_filename):
    with open(input_filename, 'r') as input_file:
        lines = input_file.readlines()
    with open(output_filename, 'w') as output_file:
        output_file.writelines(reversed(lines))


# Step 2.4.6
# Print list of folders with at least one file with extension '.py'
def search_python_files(dir_name):
    # Relative path to directory for test "data/step_2.4.6"
    os.chdir(dir_name)
    python_dirs = []
    for current_dir, dirs, files in os.walk('.'):
        for file in files:
            if file.endswith('.py'):
                python_dirs.append(current_dir.lstrip('.\\'))
                break
    print("\n".join(python_dirs))


# Step 2.5.6
# Function for generation lambda functions
def mod_checker(k, mod=0):
    return lambda x: x % k == mod
